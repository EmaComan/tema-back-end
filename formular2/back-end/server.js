const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");
var cors = require('cors');

const app = express();
app.use(cors())
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "formular2",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), email  VARCHAR(255) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10),cnp  VARCHAR(20),varsta VARCHAR(3))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});



app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
    cnp: req.body.cnp,
    gen: req.body.varsta
  };
  let error = [];
 

  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ

  if (!bilet.nume||!bilet.prenume||!bilet.telefon||!bilet.email||!bilet.data_inceput||!bilet.data_sfarsit||!bilet.cnp||!bilet.varsta) {
    error.push("Unul sau mai multe campuri obligatorii nu au fost introduse");
    console.log("Unul sau mai multe campuri obligatorii nu au fost introduse!");

  } else {
    if (bilet.nume.length < 2 || bilet.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
      
    } else if (!bilet.nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (bilet.prenume.length < 2 || bilet.nume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!bilet.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (bilet.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    
    if (!validateEmail(bilet.email)) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }

    const valid1 = validateDate(bilet.data_inceput);
    if (!valid1) {
      console.log("Data inceput invalid!");
      error.push("Data inceput invalid!");
    }
    const valid2 = validateDate(bilet.data_sfarsit);
    if (!valid2) {
      console.log("Data sfarsit invalid!");
      error.push("Data sfarsit invalid!");
    }
    if (valid1 && valid2) {
      let valid = checkDateGreaterOrEqual(bilet.data_inceput, bilet.data_sfarsit);
      if (!valid) {
        console.log("Data sfarsit trebuie sa fie mai mare decat data de inceput!");
        error.push("Data sfarsit trebuie sa fie mai mare decat data de inceput!");
      }
      const dateNow = new Date();
      valid = checkDateGreaterOrEqual(bilet.data_inceput, dateNow);
      if (!valid) {
        console.log("Data curenta trebuie sa fie mai mare decat data de inceput!");
        error.push("Data curenta trebuie sa fie mai mare decat data de inceput!");
      }
      valid = checkDateGreaterOrEqual(dateNow, bilet.data_sfarsit);
      if (!valid) {
        console.log("Data curenta trebuie sa fie mai mica decat data de sfarsit!");
        error.push("Data curenta trebuie sa fie mai mica decat data de sfarsit!");
      }
    }
    let cnpValid = true;
      if (bilet.cnp.length != 13) {
        console.log("CNP-ul trebuie sa fie de 13 cifre!");
        error.push("CNP-UL trebuie sa fie de 13 cifre!");
        cnpValid = false;
      } else if (!bilet.cnp.match("^[0-9]+$")) {
        console.log("CNP-UL trebuie sa contina doar cifre!");
        error.push("CNP-ul trebuie sa contina doar cifre!");
        cnpValid = false;
      }
      let varstaValid = true;
      if (bilet.varsta.length < 1 || bilet.varsta.length > 3) {
        console.log("Varsta invalida!");
        error.push("Varsta invalida!");
        varstaValid = false;
      } else if (!bilet.varsta.match("^[0-9]+$")) {
        console.log("Varsta trebuie sa contina doar cifre!");
        error.push("Varsta trebuie sa contina doar cifre!");
        varstaValid = false;
      }
      if (cnpValid) {
        bilet.gen = getGender(bilet.cnp);
      }
      if (cnpValid && varstaValid) {
        if (!checkAge(bilet.cnp, bilet.varsta)) {
          console.log("Varsta nu corespunde cu cea din CNP!");
          error.push("Varsta nu corespunde cu cea din CNP!");
        }
      }
  if (error.length === 0) {
    const sql = `select nume from abonamente_metrou where email = ? limit 1;`;
    connection.query(
      sql,
      [
        bilet.email
      ],
      function (err, result) {
        if (err) throw err;
        console.log(result);
        if (result && result[0] && result[0].nume) {
          console.log("Emailul exista deja in baza de date!");
          error.push("Emailul exista deja in baza de date!");
          res.status(500).send(error);
          console.log("Abonamentul nu a putut fi creat!");
        }
        else {
          const sql = `INSERT INTO abonamente_metrou (nume,
            prenume,
            telefon,
            cnp,
            email,
            data_inceput,
            data_sfarsit,
            varsta,
            Gen) VALUES (?,?,?,?,?,?,?,?,?)`;
          connection.query(
            sql,
            [
              bilet.nume,
              bilet.prenume,
              bilet.telefon,
              bilet.cnp,
              bilet.email,
              bilet.data_inceput,
              bilet.data_sfarsit,
              bilet.varsta,
              bilet.gen
            ],
            function (err, result) {
              if (err) throw err;
              console.log("Abonament realizat cu succes!");
              res.status(200).send({
                message: "Abonament realizat cu succes",
              });
              console.log(sql);
            }
          );
        }
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
}});

function validateDate(date) {
  if (!date) { return false; }
  const dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/;
  // Match the date format through regular expression
  if(date.match(dateformat))
  {
    return true;
  }
  return false;
}
function checkDateGreaterOrEqual(date1, date2) {
  if (!date1 || !date2) { return false; }
  const d1 = new Date(date1).setHours(0, 0, 0, 0);
  const d2 = new Date(date2).setHours(0, 0, 0, 0);
  return d1 <= d2; 
}

function getGender(cnp) {
  if (!cnp || !cnp[0]) { return null; }
  const g = parseInt(cnp[0]);
  if ([1, 3, 5].indexOf(g) > -1) {
    return 'M';
  }
  else if ([2, 4, 6].indexOf(g) > -1) {
    return 'F';
  }
  return null;
}

function checkAge(cnp, age) {
  if (!cnp || !cnp.length) { return false; }
  let year = parseInt(cnp[1] + cnp[2]);
  let currentYear = parseInt((new Date()).getFullYear().toString().substring(2));
  if (year >= 0 && year <= currentYear) {
    year = 2000 + year;
  }
  else if (year > currentYear) {
    year = 1900 + year;
  }
  currentYear = 2000 + currentYear;
  return parseInt(age) === (currentYear - year);
}
function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

//modifica si din front la index.html

